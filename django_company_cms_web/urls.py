from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="web"),
    path("domains-and-dns", views.domains_and_dns, name="web_domains_and_dns"),
    path("tld/<str:tld>", views.tld_info, name="web_tld_info"),
]
