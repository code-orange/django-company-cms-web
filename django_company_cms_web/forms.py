from django import forms
from django.utils.translation import gettext_lazy as _

from django_mdat_tld.django_mdat_tld.models import *


class DomainCheckForm(forms.Form):
    name = forms.CharField(max_length=100)
    tld = forms.ModelChoiceField(
        label=_("Top-Level-Domain"),
        queryset=MdatTopLevelDomains.objects.all().select_related(),
    )
