from datetime import datetime

from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from django_mdat_tld.django_mdat_tld.models import *
from . import views


class WebStaticViewSitemap(Sitemap):
    def items(self):
        return ["web", "web_domains_and_dns", "web_tld_info"]

    def location(self, item):
        return reverse(item)


class WebTopLevelDomainsSitemap(Sitemap):
    def items(self):
        return MdatTopLevelDomains.objects.all()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(views.tld_info, args=[obj.name.lower()])


class WebTopLevelDomainsPunycodeSitemap(Sitemap):
    def items(self):
        return MdatTopLevelDomains.objects.filter(punycode__isnull=False).all()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(views.tld_info, args=[obj.punycode.lower()])
