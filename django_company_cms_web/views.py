from django.http import HttpResponse, HttpResponseNotFound
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)
from django_company_cms_web.django_company_cms_web.forms import DomainCheckForm
from django_mdat_tld.django_mdat_tld.models import *


def home(request):
    template = loader.get_template("django_company_cms_web/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Webhosting")
    template_opts["content_title_sub"] = _("Up & Running")

    template_opts["product_web_home_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_web_home_01_header"
    ).content

    template_opts["product_web_home_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_web_home_01_content"
    ).content

    template_opts["product_web_home_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_web_home_02_header"
    ).content

    template_opts["product_web_home_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_web_home_02_content"
    ).content

    template_opts["product_web_home_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_web_home_03_header"
    ).content

    template_opts["product_web_home_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_web_home_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def domains_and_dns(request):
    template = loader.get_template("django_company_cms_web/domains_and_dns.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Webhosting")
    template_opts["content_title_sub"] = _("Domains & DNS")

    template_opts["product_domains_and_dns_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_01_header"
    ).content

    template_opts["product_domains_and_dns_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_01_content"
    ).content

    template_opts["product_domains_and_dns_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_02_header"
    ).content

    template_opts["product_domains_and_dns_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_02_content"
    ).content

    template_opts["product_domains_and_dns_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_03_header"
    ).content

    template_opts["product_domains_and_dns_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_03_content"
    ).content

    template_opts["product_domains_and_dns_04_header"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_04_header"
    ).content

    template_opts["product_domains_and_dns_04_content"] = CompanyCmsGeneral.objects.get(
        name="product_domains_and_dns_04_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def tld_info(request, tld: str):
    template = loader.get_template("django_company_cms_web/tld_info.html")

    form_initial = dict()

    template_opts = dict()

    template_opts["content_title_main"] = _("Domain Info")
    template_opts["content_title_sub"] = _("Overview")

    try:
        tld_data = MdatTopLevelDomains.objects.get(name=tld)
    except MdatTopLevelDomains.DoesNotExist:
        try:
            tld_data = MdatTopLevelDomains.objects.get(punycode=tld)
        except MdatTopLevelDomains.DoesNotExist:
            return HttpResponseNotFound()

    template_opts["content_title_sub"] = "." + tld_data.name

    form_initial["tld"] = tld_data

    # Form
    domain_check_form = DomainCheckForm(initial=form_initial)

    template_opts["domain_check_form"] = domain_check_form

    return HttpResponse(template.render(template_opts, request))
